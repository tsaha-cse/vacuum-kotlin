package tushar.vacuum.validation

import org.junit.Before
import org.junit.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue


/**
 * Created by TUSHAR on 3/7/18.
 * tsaha.cse@gmail.com
 */
class ValidationTest {

    lateinit var validation: Validation

    @Before
    fun setup() {
        validation = ValidationImpl()
    }

    @Test
    fun isNameValid_ShouldReturnTrue() {
        assertTrue(validation.isNameValid("Tushar Saha"))
        assertTrue(validation.isNameValid("T S"))
        assertTrue(validation.isNameValid("Juan Calderon Zumba"))
    }

    @Test
    fun isNameValid_ShouldReturnFalse() {
        assertFalse(validation.isNameValid(""))
        assertFalse(validation.isNameValid("TusharSaha"))
        // Last name not included
        assertFalse(validation.isNameValid("Juan"))
        // Last name not included and ended with whitespace
        assertFalse(validation.isNameValid("Tushar "))
    }

    @Test
    fun isNameAvailableForSignup_ShouldReturnFalse() {
        assertFalse(validation.isNameAvailableForSignUp("Rodney"))
        // reserved name ended with a white space
        assertFalse(validation.isNameAvailableForSignUp("Oliva "))
    }

    @Test
    fun isNameAvailableForSignUp_ShouldReturnTrue() {
        assertTrue(validation.isNameAvailableForSignUp("Rodney Oliva"))
        assertTrue(validation.isNameAvailableForSignUp("Oliva Rodney"))
    }
}