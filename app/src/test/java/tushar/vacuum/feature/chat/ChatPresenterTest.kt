package tushar.vacuum.feature.chat

import io.reactivex.Scheduler
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito.atLeastOnce
import org.mockito.MockitoAnnotations
import tushar.vacuum.di.RunOn
import tushar.vacuum.di.SchedulerType
import tushar.vacuum.repository.DataRepository
import tushar.vacuum.session.Session
import tushar.vacuum.validation.Validation
import javax.inject.Named


/**
 * Created by TUSHAR on 2/23/18.
 * tsaha.cse@gmail.com
 */

class ChatPresenterTest {

    @Named("shared_preference")
    @Mock
    internal var session: Session? = null

    @Mock
    internal var validation: Validation? = null

    @Mock
    internal var dataRepository: DataRepository? = null

    @Mock
    @RunOn(SchedulerType.IO)
    internal var ioScheduler: Scheduler? = null

    @Mock
    @RunOn(SchedulerType.UI)
    internal var uiScheduler: Scheduler? = null

    @Mock
    private val view: ChatContract.ChatView? = null

    private var presenter: ChatPresenter? = null

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = ChatPresenter(dataRepository!!, ioScheduler!!, uiScheduler!!, session!!)
        presenter!!.attachView(view!!)
    }

    @Test
    fun viewAttached_whenAlreadyLoggedIn_shouldOpenChatActivity() {
        // given
        given(session!!.isLoggedIn()).willReturn(false)
        presenter!!.onPresent()
        // Then
        then<ChatContract.ChatView>(view).should(atLeastOnce()).onLogout()
    }
}