package tushar.vacuum.feature.login

import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito.atLeastOnce
import org.mockito.MockitoAnnotations
import tushar.vacuum.session.Session
import tushar.vacuum.validation.Validation
import javax.inject.Named


/**
 * Created by TUSHAR on 3/7/18.
 * tsaha.cse@gmail.com
 */

class LoginPresenterTest {
    @Named("shared_preference")
    @Mock
    lateinit var session: Session

    @Mock
    lateinit var validation: Validation

    @Mock
    private lateinit var view: LoginContract.LoginView

    private lateinit var presenter: LoginPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = LoginPresenter(session, validation)
        presenter.attachView(view)
    }

    @Test
    fun viewAttached_whenAlreadyLoggedIn_shouldOpenChatActivity() {
        // given
        given(session.isLoggedIn()).willReturn(true)
        presenter.onViewAttached()
        // Then
        then<LoginContract.LoginView>(view).should(atLeastOnce()).gotoChatActivity()
    }
}