package tushar.vacuum.repository

import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import org.hamcrest.Matchers.empty
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import tushar.vacuum.model.Chat
import java.util.*
import kotlin.test.assertEquals


/**
 * Created by TUSHAR on 3/7/18.
 * tsaha.cse@gmail.com
 */

class DataRepositoryTest {
    @Mock
    @Local
    lateinit var localDataSource: DataSource
    @Mock
    @Remote
    lateinit var remoteDataSource: DataSource

    lateinit var dataRepository: DataRepository
    lateinit var chatsTestSubscriber: TestSubscriber<List<Chat>>

    companion object {
        private val chat1 = Chat()
        private val chat2 = Chat()
        private val chat3 = Chat()
        private val chats = Arrays.asList<Chat>(chat1, chat2, chat3)
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        dataRepository = DataRepository(localDataSource, remoteDataSource)
        chatsTestSubscriber = TestSubscriber<List<Chat>>()
    }

    @Test
    fun loadChats_ShouldReturnCache_IfItIsAvailable() {
        // Given cache
        dataRepository.cache.addAll(chats)
        dataRepository.loadData(false).subscribe(chatsTestSubscriber)
        // verify that there is nothing to do
        // with local or remote sources
        verifyZeroInteractions(localDataSource)
        verifyZeroInteractions(remoteDataSource)
        chatsTestSubscriber.assertValue(chats)
    }

    @Test
    fun loadChats_ShouldReturnFromLocal_IfCacheIsUnavailable() {
        doReturn(Flowable.just<List<Chat>>(chats)).`when`(localDataSource).loadData(false)
        doReturn(Flowable.just<List<Chat>>(chats)).`when`(remoteDataSource).loadData(true)
        dataRepository.loadData(false).subscribe(chatsTestSubscriber)
        // Loads from local source
        verify<DataSource>(localDataSource).loadData(false)
        // Loads from remote source if local data is unavailable
        verify<DataSource>(remoteDataSource).loadData(true)
        chatsTestSubscriber.assertValue(chats)
    }

    @Test
    fun loadChats_ShouldReturnFromRemote_WhenNeeded() {
        doReturn(Flowable.just<List<Chat>>(chats)).`when`(remoteDataSource).loadData(true)
        // Force remote
        dataRepository.loadData(true).subscribe(chatsTestSubscriber)
        // Remote get called
        verify<DataSource>(remoteDataSource).loadData(true)
        // Local never called
        verify<DataSource>(localDataSource, never()).loadData(true)
        // Local data are cleared
        verify<DataSource>(localDataSource).clearData()
        // Caches are same as given chats
        assertEquals(dataRepository.cache, chats)
        // Local's add data get called
        verify(localDataSource, times(3)).addData(chat1)
        verify(localDataSource, times(3)).addData(chat1)
        verify(localDataSource, times(3)).addData(chat2)
        verify(localDataSource, times(3)).addData(chat3)
        chatsTestSubscriber.assertValue(chats)
    }

    @Test
    fun clearChats_ShouldClearCachesAndLocalData() {
        // Given cache
        dataRepository.cache.addAll(chats)
        dataRepository.clearData()
        // Cache should be empty
        assertThat(dataRepository.cache, empty<Any>())
        // Local data get empty
        then<DataSource>(localDataSource).should().clearData()
    }
}