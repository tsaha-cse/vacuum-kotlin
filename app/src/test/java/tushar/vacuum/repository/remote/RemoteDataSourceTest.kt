package tushar.vacuum.repository.remote

import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations
import tushar.vacuum.api.ApiService
import tushar.vacuum.api.ChatResponse
import tushar.vacuum.model.Chat


/**
 * Created by TUSHAR on 3/7/18.
 * tsaha.cse@gmail.com
 */

class RemoteDataSourceTest {

    private lateinit var remoteDataSource: RemoteDataSource

    @Mock
    lateinit var apiService: ApiService

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        remoteDataSource = RemoteDataSource(apiService)
    }

    @Test
    fun loadChats_ShouldReturnFromRemoteService() {
        val chatResponse = mock<ChatResponse>(ChatResponse::class.java)
        given(apiService.getChat()).willReturn(Flowable.just<ChatResponse>(chatResponse))
    }

    @Test(expected = UnsupportedOperationException::class)
    fun addChats_NoThingToDoWithRemoteSource() {
        val chat = mock<Chat>(Chat::class.java)
        remoteDataSource.addData(chat)
        // should not have to do anything with ApiService
        then<ApiService>(apiService).shouldHaveZeroInteractions()
    }

    @Test(expected = UnsupportedOperationException::class)
    fun clearChats_NoThingToDoWithRemoteSource() {
        remoteDataSource.clearData()
        // should not have to do anything with ApiService
        then<ApiService>(apiService).shouldHaveZeroInteractions()
    }
}