package tushar.vacuum.repository.local

import io.reactivex.Flowable
import io.reactivex.subscribers.TestSubscriber
import org.junit.Before
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.BDDMockito.then
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import tushar.vacuum.model.Chat
import tushar.vacuum.repository.local.db.ChatDao
import java.util.*


/**
 * Created by TUSHAR on 3/7/18.
 * tsaha.cse@gmail.com
 */

class LocalDataSourceTest {

    companion object {
        private val chat1 = Chat()
        private val chat2 = Chat()
        private val chat3 = Chat()
        private val chats = Arrays.asList<Chat>(chat1, chat2, chat3)
    }

    lateinit var localDataSource: LocalDataSource

    @Mock
    lateinit var chatDao: ChatDao

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        localDataSource = LocalDataSource(chatDao)
    }

    @Test
    fun loadChats_ShouldReturnFromDatabase() {
        val subscriber = TestSubscriber<List<Chat>>()
        given(chatDao.getAllChat()).willReturn(Flowable.just<List<Chat>>(chats))
        localDataSource.loadData(false).subscribe(subscriber)
        // should called
        then<ChatDao>(chatDao).should().getAllChat()
    }

    @Test
    fun addChats_ShouldInsertToDatabase() {
        val chat = Chat()
        localDataSource.addData(chat)
        // should called
        then<ChatDao>(chatDao).should().insert(chat)
    }

    @Test
    fun clearChats_ShouldDeleteAllDataInDatabase() {
        localDataSource.clearData()
        // should called
        then<ChatDao>(chatDao).should().deleteAll()
    }
}