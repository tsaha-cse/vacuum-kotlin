package tushar.vacuum.di

import android.app.Application
import dagger.Module
import dagger.Provides
import tushar.vacuum.App
import tushar.vacuum.validation.Validation
import tushar.vacuum.validation.ValidationImpl
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApp(): App {
        return application as App
    }

    @Provides
    @Singleton
    fun provideValidation(): Validation {
        return ValidationImpl()
    }
}