package tushar.vacuum.di

import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import tushar.vacuum.App
import tushar.vacuum.session.Session
import tushar.vacuum.session.SharedPreferenceSessionImpl
import javax.inject.Named
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Module
class SessionModule {

    @Provides
    @Singleton
    fun provideSharedPreference(app: App): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app)
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Named("shared_preference")
    @Provides
    @Singleton
    fun provideSession(preferences: SharedPreferences, gson: Gson): Session {
        return SharedPreferenceSessionImpl(preferences, gson)
    }
}