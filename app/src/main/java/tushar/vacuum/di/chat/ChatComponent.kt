package tushar.vacuum.di.chat

import dagger.Subcomponent
import tushar.vacuum.feature.chat.ChatActivity
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Singleton
@Subcomponent(modules = [(ChatModule::class)])
interface ChatComponent {
    fun inject(activity: ChatActivity)
}