package tushar.vacuum.di

import dagger.Component
import tushar.vacuum.di.chat.ChatComponent
import tushar.vacuum.di.chat.ChatModule
import tushar.vacuum.di.login.LoginComponent
import tushar.vacuum.di.login.LoginModule
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Singleton
@Component(modules = [(AppModule::class), (RetrofitModule::class), (DbModule::class), (DataSourceModule::class),
(SchedulerModule::class), (SessionModule::class)])
interface AppComponent {
    operator fun plus(module: LoginModule): LoginComponent

    operator fun plus(module: ChatModule): ChatComponent
}