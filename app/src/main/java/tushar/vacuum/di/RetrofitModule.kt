package tushar.vacuum.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import tushar.vacuum.BuildConfig
import tushar.vacuum.api.ApiService
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Module
class RetrofitModule {

    @Named("api_url")
    @Provides
    @Singleton
    fun getApiUrl(): String {
        return BuildConfig.API_URL
    }

    @Named("timeout_session")
    @Provides
    @Singleton
    fun getTimeOutDuration(): Int {
        return 30
    }

    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor,
                            @Named("timeout_session") timeoutSession: Int): OkHttpClient {
        val okhttpClientBuilder = OkHttpClient.Builder()
        okhttpClientBuilder.addInterceptor(loggingInterceptor)
        okhttpClientBuilder.connectTimeout(timeoutSession.toLong(), TimeUnit.SECONDS)
        okhttpClientBuilder.readTimeout(timeoutSession.toLong(), TimeUnit.SECONDS)
        okhttpClientBuilder.writeTimeout(timeoutSession.toLong(), TimeUnit.SECONDS)
        return okhttpClientBuilder.build()
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideRxJavaAdapterFactory(): CallAdapter.Factory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    @Singleton
    fun provideRetrofit(@Named("api_url") url: String, okHttpClient: OkHttpClient, gsonConverter: Converter.Factory,
                        rxCallAdapter: CallAdapter.Factory): Retrofit {
        return Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(gsonConverter)
                .addCallAdapterFactory(rxCallAdapter)
                .build()
    }

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create<ApiService>(ApiService::class.java)
    }
}