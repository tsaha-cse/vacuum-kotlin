package tushar.vacuum.di

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Module
class SchedulerModule {

    @Provides
    @RunOn(SchedulerType.IO)
    fun provideIo(): Scheduler {
        return Schedulers.io()
    }

    @Provides
    @RunOn(SchedulerType.UI)
    fun provideUi(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}