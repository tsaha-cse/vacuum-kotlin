package tushar.vacuum.di

import android.arch.persistence.room.Room
import dagger.Module
import dagger.Provides
import tushar.vacuum.App
import tushar.vacuum.BuildConfig
import tushar.vacuum.repository.local.db.ChatDao
import tushar.vacuum.repository.local.db.ChatDb
import javax.inject.Named
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Module
class DbModule {

    @Provides
    @Named("database_name")
    fun provideDatabaseName(): String {
        return BuildConfig.DB_NAME
    }

    @Provides
    @Singleton
    fun provideUserDao(app: App, @Named("database_name") dbName: String): ChatDb {
        return Room.databaseBuilder(app, ChatDb::class.java, dbName).build()
    }

    @Provides
    @Singleton
    fun provideQuestionDao(chatDb: ChatDb): ChatDao {
        return chatDb.chatDao()
    }
}