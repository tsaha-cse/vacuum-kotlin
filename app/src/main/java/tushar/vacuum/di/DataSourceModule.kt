package tushar.vacuum.di

import dagger.Module
import dagger.Provides
import tushar.vacuum.repository.DataSource
import tushar.vacuum.repository.Local
import tushar.vacuum.repository.Remote
import tushar.vacuum.repository.local.LocalDataSource
import tushar.vacuum.repository.remote.RemoteDataSource
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Module
class DataSourceModule {

    @Provides
    @Local
    @Singleton
    fun provideLocalDataSource(localDataSource: LocalDataSource): DataSource {
        return localDataSource
    }

    @Provides
    @Remote
    @Singleton
    fun provideRemoteDataSource(remoteDataSource: RemoteDataSource): DataSource {
        return remoteDataSource
    }
}