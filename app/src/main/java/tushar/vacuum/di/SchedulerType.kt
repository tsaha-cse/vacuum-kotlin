package tushar.vacuum.di


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

enum class SchedulerType {
    IO, UI
}