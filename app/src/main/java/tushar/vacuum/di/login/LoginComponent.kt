package tushar.vacuum.di.login

import dagger.Subcomponent
import tushar.vacuum.feature.login.LoginActivity
import javax.inject.Singleton


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Singleton
@Subcomponent(modules = [(LoginModule::class)])
interface LoginComponent {
    fun inject(activity: LoginActivity)
}