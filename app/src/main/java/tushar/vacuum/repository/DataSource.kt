package tushar.vacuum.repository

import io.reactivex.Flowable
import tushar.vacuum.model.Chat

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

interface DataSource {
    fun loadData(forceRemote: Boolean): Flowable<List<Chat>>

    fun addData(chat: Chat)

    fun clearData()
}