package tushar.vacuum.repository.remote

import io.reactivex.Flowable
import tushar.vacuum.api.ApiService
import tushar.vacuum.api.ChatResponse
import tushar.vacuum.model.Chat
import tushar.vacuum.repository.DataSource
import javax.inject.Inject

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class RemoteDataSource @Inject constructor(apiService: ApiService) : DataSource {

    private var apiService: ApiService = apiService

    override fun loadData(forceRemote: Boolean): Flowable<List<Chat>> {
        return apiService.getChat().map(ChatResponse::getChats)
    }

    override fun addData(chat: Chat) {
        throw UnsupportedOperationException("Remote source doesn't support insert operation")
    }

    override fun clearData() {
        throw UnsupportedOperationException("Remote source doesn't support insert operation")
    }
}