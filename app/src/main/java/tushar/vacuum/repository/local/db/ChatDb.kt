package tushar.vacuum.repository.local.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import tushar.vacuum.model.Chat

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Database(entities = [(Chat::class)], version = 1, exportSchema = false)
abstract class ChatDb : RoomDatabase() {
    abstract fun chatDao(): ChatDao
}