package tushar.vacuum.repository.local.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import tushar.vacuum.BuildConfig
import tushar.vacuum.model.Chat

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Dao
interface ChatDao {

    @Query("SELECT * FROM ${BuildConfig.TABLE_NAME}")
    fun getAllChat(): Flowable<List<Chat>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chat: Chat)

    @Query("DELETE FROM ${BuildConfig.TABLE_NAME}")
    fun deleteAll()
}