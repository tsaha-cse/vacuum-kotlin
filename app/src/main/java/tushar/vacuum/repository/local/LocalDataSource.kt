package tushar.vacuum.repository.local

import io.reactivex.Flowable
import tushar.vacuum.model.Chat
import tushar.vacuum.repository.DataSource
import tushar.vacuum.repository.local.db.ChatDao
import javax.inject.Inject

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class LocalDataSource @Inject constructor(chatDao: ChatDao) : DataSource {

    private var chatDao: ChatDao = chatDao

    override fun loadData(forceRemote: Boolean): Flowable<List<Chat>> {
        return chatDao.getAllChat()
    }

    override fun addData(chat: Chat) {
        chatDao.insert(chat)
    }

    override fun clearData() {
        chatDao.deleteAll()
    }
}