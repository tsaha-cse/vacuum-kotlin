package tushar.vacuum.repository

import java.lang.annotation.Documented
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Qualifier
@Documented
@Retention(RetentionPolicy.RUNTIME)
annotation class Local