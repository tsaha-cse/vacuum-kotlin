package tushar.vacuum.repository

import io.reactivex.Flowable
import tushar.vacuum.model.Chat
import javax.inject.Inject

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class DataRepository @Inject constructor(@Local localDataSource: DataSource, @Remote remoteDataSource: DataSource) : DataSource {

    private var localSource: DataSource = localDataSource
    private var remoteSource: DataSource = remoteDataSource

    var cache: MutableList<Chat> = ArrayList()

    override fun loadData(forceRemote: Boolean): Flowable<List<Chat>> {
        if (forceRemote) {
            return getFreshData()
        } else {
            return if (cache.size > 0) {
                // cache is available, return it immediately
                Flowable.just(cache)
            } else {
                localSource.loadData(false)
                        .take(1)
                        .flatMap { source -> Flowable.fromIterable(source) }
                        .doOnNext { chat -> cache.add(chat) }
                        .toList()
                        .toFlowable()
                        .filter { list -> !list.isEmpty() }
                        .switchIfEmpty(getFreshData())
            }
        }
    }

    override fun addData(chat: Chat) {
        localSource.addData(chat)
        cache.add(chat)
    }

    override fun clearData() {
        cache.clear()
        localSource.clearData()
    }

    private fun getFreshData() : Flowable<List<Chat>> {
        return remoteSource
                .loadData(true)
                .doOnNext {
                    cache.clear()
                    localSource.clearData()
                }
                .flatMap { source -> Flowable.fromIterable(source) }
                .doOnNext { chat ->
                    // adding to on memory cache
                    cache.add(chat)
                    // adding to db
                    localSource.addData(chat)
                }
                .toList()
                .toFlowable()
    }
}