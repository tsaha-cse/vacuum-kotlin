package tushar.vacuum

import android.app.Application
import tushar.vacuum.di.AppComponent
import tushar.vacuum.di.AppModule
import tushar.vacuum.di.DaggerAppComponent


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class App : Application() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    fun getAppComponent(): AppComponent {
        return appComponent
    }
}