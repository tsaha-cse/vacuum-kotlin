package tushar.vacuum.api

import com.google.gson.annotations.SerializedName
import tushar.vacuum.model.Chat

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */
data class ChatResponse(
        @SerializedName("chats")
        private var chats: List<Chat>) {

    fun getChats(): List<Chat>? {
        return chats
    }
}