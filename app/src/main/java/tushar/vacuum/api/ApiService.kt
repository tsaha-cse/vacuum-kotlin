package tushar.vacuum.api

import io.reactivex.Flowable
import retrofit2.http.GET

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

interface ApiService {

    @GET("rocket-interview/chat.json")
    fun getChat(): Flowable<ChatResponse>
}