package tushar.vacuum.session

import tushar.vacuum.model.User

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

interface Session {

    fun isLoggedIn(): Boolean

    fun saveUser(user: User)

    fun getUser(): User?

    fun flush()
}