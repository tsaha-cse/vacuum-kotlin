package tushar.vacuum.session

import android.content.SharedPreferences
import com.google.gson.Gson
import tushar.vacuum.model.User

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class SharedPreferenceSessionImpl(sharedPreference : SharedPreferences, gson : Gson) : Session {

    private val KEY_USER = "user"
    private var sharedPreferences: SharedPreferences = sharedPreference
    private var gson: Gson = gson
    private var user: User? = null

    override fun isLoggedIn(): Boolean {
        return getUser() != null
    }

    override fun saveUser(user: User) {
        putString(KEY_USER, gson.toJson(user))
        this.user = user
    }

    override fun getUser(): User? {
        if (user == null) {
            user = gson.fromJson(getString(KEY_USER), User::class.java)
        }
        return user
    }

    override fun flush() {
        removeKey(KEY_USER)
        user = null
    }

    private fun putString(key: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(key, value).apply()
    }

    private fun getString(key: String): String? {
        return sharedPreferences.getString(key, null)
    }

    private fun removeKey(key: String) {
        val editor = sharedPreferences.edit()
        editor.remove(key).apply()
    }
}