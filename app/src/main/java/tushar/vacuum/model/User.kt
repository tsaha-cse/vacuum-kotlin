package tushar.vacuum.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */
data class User(
        @Expose
        @SerializedName("id")
        val id: String,

        @Expose
        @SerializedName("full_name")
        val fullName: String,

        @Expose
        @SerializedName("created_at")
        val createdAt: Long) {

    companion object {
        fun build(fullName: String): User {
            return User(
                    UUID.randomUUID().toString(),
                    fullName,
                    System.currentTimeMillis())
        }
    }
}