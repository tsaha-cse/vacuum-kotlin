package tushar.vacuum.model

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

enum class UserType constructor(type: Int) {
    OWN(1),
    OTHER(2);

    private var type = 2 // default OTHER

    init {
        this.type = type
    }

    fun getType() : Int {
        return type
    }
}