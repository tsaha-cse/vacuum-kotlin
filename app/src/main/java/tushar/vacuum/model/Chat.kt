package tushar.vacuum.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import tushar.vacuum.BuildConfig

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

@Entity(tableName = BuildConfig.TABLE_NAME)
data class Chat(
        @PrimaryKey(autoGenerate = true)
        var id: Long = 0,

        @ColumnInfo(name = "user_type")
        var userType: Int = UserType.OTHER.getType(),

        @Expose
        @SerializedName("username")
        @ColumnInfo(name = "name")
        var username: String? = null,

        @Expose
        @SerializedName("content")
        @ColumnInfo(name = "content")
        var content: String? = null,

        @Expose
        @SerializedName("userImage_url")
        @ColumnInfo(name = "image_url")
        var userImageUrl: String = "",

        @Expose
        @SerializedName("time")
        @ColumnInfo(name = "time")
        var time: String? = null) {


    companion object {
        /**
         * build OWN type chat message
         */
        fun build(username: String, content: String, time: String): Chat {
            return Chat().apply {
                this.userType = UserType.OWN.getType()
                this.username = username
                this.content = content
                this.time = time
            }
        }
    }

}