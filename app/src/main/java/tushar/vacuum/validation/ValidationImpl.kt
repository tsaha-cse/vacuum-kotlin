package tushar.vacuum.validation

import java.util.regex.Pattern


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class ValidationImpl : Validation {

    override fun isEmpty(fullName: String): Boolean {
        return isNullOrEmpty(fullName)
    }

    override fun isNameValid(fullName: String): Boolean {
        return Pattern
                .compile("([A-Za-z]+\\s+['A-Za-z]).{0,50}", Pattern.CASE_INSENSITIVE)
                .matcher(fullName)
                .matches()
    }

    override fun isNameAvailableForSignUp(fullName: String): Boolean {
        val existNames = arrayOf("Carrie", "Anthony", "Eleanor", "Rodney", "Oliva", "Merve", "Lily")
        for (existName in existNames) {
            if (existName.toLowerCase() == fullName.trim().toLowerCase()) {
                return false
            }
        }
        return true
    }

    override fun isNullOrEmpty(cs: CharSequence?): Boolean {
        return cs == null || isEmpty(cs)
    }

    private fun isEmpty(cs: CharSequence): Boolean {
        return cs.isEmpty()
    }
}
