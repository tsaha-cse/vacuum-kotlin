package tushar.vacuum.validation


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

interface Validation {

    fun isEmpty(fullName: String): Boolean

    fun isNameValid(fullName: String): Boolean

    fun isNameAvailableForSignUp(fullName: String): Boolean

    fun isNullOrEmpty(cs: CharSequence?): Boolean
}