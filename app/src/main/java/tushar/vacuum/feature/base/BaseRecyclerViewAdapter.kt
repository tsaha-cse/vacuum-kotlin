package tushar.vacuum.feature.base

import android.support.v7.widget.RecyclerView
import java.util.*


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

abstract class BaseRecyclerViewAdapter<T, VH : RecyclerView.ViewHolder> protected constructor(private var objects: MutableList<T>?) : RecyclerView.Adapter<VH>() {

    private var notifyOnChange = true

    private val lock = Any()

    val allItems: List<T>?
        get() = objects

    override fun getItemCount(): Int {
        return if (objects != null) objects!!.size else 0
    }

    fun getItem(position: Int): T? {
        return if (objects == null) {
            null
        } else objects!![position]

    }

    fun add(`object`: T) {
        synchronized(lock) {
            if (objects == null) {
                objects = ArrayList()
            }

            objects!!.add(`object`)
        }

        if (notifyOnChange) {
            notifyItemInserted(objects!!.size - 1)
        }
    }

    fun addAll(collection: Collection<T>?) {
        if (collection != null) {
            synchronized(lock) {
                if (objects == null) {
                    objects = ArrayList()
                }

                objects!!.addAll(collection)
            }

            if (notifyOnChange) {
                if (objects!!.size - collection.size != 0) {
                    notifyItemRangeChanged(objects!!.size - collection.size, collection.size)
                } else {
                    notifyDataSetChanged()
                }
            }
        }
    }

    @SafeVarargs
    fun addAll(vararg objects: T) {
        synchronized(lock) {
            if (this.objects == null) {
                this.objects = ArrayList()
            }

            Collections.addAll(this.objects!!, *objects)
        }

        if (notifyOnChange) {
            if (this.objects!!.size - objects.size != 0) {
                notifyItemRangeChanged(this.objects!!.size - objects.size, objects.size)
            } else {
                notifyDataSetChanged()
            }
        }
    }

    fun insert(`object`: T, index: Int) {
        synchronized(lock) {
            if (objects == null) {
                objects = ArrayList()
            }

            objects!!.add(index, `object`)
        }

        if (notifyOnChange) {
            notifyItemInserted(index)
        }
    }

    fun remove(`object`: T) {
        var removeIndex = 0

        synchronized(lock) {
            if (objects == null) {
                return
            }

            removeIndex = objects!!.indexOf(`object`)

            if (removeIndex != -1) {
                objects!!.removeAt(removeIndex)
            }
        }

        if (notifyOnChange && removeIndex != -1) {
            notifyDataSetChanged()
        }
    }

    fun clear() {
        synchronized(lock) {
            if (objects == null) {
                return
            }

            objects!!.clear()
        }

        if (notifyOnChange) {
            notifyDataSetChanged()
        }
    }

    fun sort(comparator: Comparator<in T>) {
        synchronized(lock) {
            if (objects == null) {
                return
            }

            Collections.sort(objects!!, comparator)
        }

        if (notifyOnChange) {
            notifyDataSetChanged()
        }
    }

    fun setNotifyOnChange(notifyOnChange: Boolean) {
        this.notifyOnChange = notifyOnChange
    }
}