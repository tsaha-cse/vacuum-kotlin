package tushar.vacuum.feature.base

import android.app.Activity
import android.content.Intent
import android.support.annotation.LayoutRes
import android.support.v4.content.ContextCompat
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.squareup.picasso.Picasso
import tushar.vacuum.R


/**
 * Created by TUSHAR on 3/7/18.
 * tsaha.cse@gmail.com
 */

inline fun <reified T> Activity.open() {
    startActivity(Intent(this, T::class.java))
}

fun ImageView.setImageFromUrl(url: String) {
    Picasso.with(context)
            .load(url)
            .placeholder(R.color.chat_other)
            .error(ContextCompat.getDrawable(context, R.color.chat_other))
            .into(this)
}

inline fun Activity.toast(text: String, length: Int = android.widget.Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, length).show();
}

inline fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

inline fun String.tryDate(): String {
    return try {
        DateUtils.getRelativeTimeSpanString(java.lang.Long.valueOf(this), System.currentTimeMillis(),
                DateUtils.MINUTE_IN_MILLIS).toString()
    } catch (e: NumberFormatException) {
        this
    }
}