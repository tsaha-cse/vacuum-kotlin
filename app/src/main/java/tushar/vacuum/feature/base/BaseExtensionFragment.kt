package tushar.vacuum.feature.base

import android.app.ProgressDialog
import android.os.Bundle
import android.support.annotation.CallSuper
import android.view.View
import butterknife.ButterKnife
import butterknife.Unbinder
import tushar.arch.BaseContract
import tushar.arch.BaseFragment
import tushar.vacuum.App


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

abstract class BaseExtensionFragment<V : BaseContract.View, P : BaseContract.Presenter<V>> : BaseFragment<V, P>() {

    protected lateinit var app: App
    private var pd: ProgressDialog? = null
    private var unbinder: Unbinder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = activity?.application as App
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        unbinder = ButterKnife.bind(view)
    }

    override fun showLoading() {
        pd = ProgressDialog(activity).apply {
            setProgressStyle(ProgressDialog.STYLE_SPINNER)
            setMessage("Please Wait...")
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            show()
        }
    }

    override fun hideLoading() {
        pd?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    override fun showToast(message: String) {
        activity?.toast(message)
    }

    @CallSuper
    override fun onDestroyView() {
        unbinder?.unbind()
        super.onDestroyView()
    }
}
