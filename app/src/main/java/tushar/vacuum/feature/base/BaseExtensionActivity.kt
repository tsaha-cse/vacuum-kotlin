package tushar.vacuum.feature.base

import android.app.ProgressDialog
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.view.MenuItem
import butterknife.ButterKnife
import tushar.arch.BaseActivity
import tushar.arch.BaseContract
import tushar.vacuum.App


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

abstract class BaseExtensionActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> : BaseActivity<V, P>() {

    protected lateinit var app: App
    private var pd: ProgressDialog? = null

    @LayoutRes
    protected abstract fun getLayoutResId(): Int

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        app = application as App
        super.onCreate(savedInstanceState)
        setContentView(getLayoutResId())
        ButterKnife.bind(this)
        init()
    }

    protected abstract fun init()

    override fun showLoading() {
        pd = ProgressDialog(this).apply {
            setProgressStyle(ProgressDialog.STYLE_SPINNER)
            setMessage("Please Wait...")
            setCancelable(false)
            setCanceledOnTouchOutside(false)
            show()
        }
    }

    override fun hideLoading() {
        pd?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    override fun showToast(message: String) {
        runOnUiThread {
            toast(message)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}