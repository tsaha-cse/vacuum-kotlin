package tushar.vacuum.feature.login

import tushar.arch.BasePresenter
import tushar.vacuum.model.User
import tushar.vacuum.session.Session
import tushar.vacuum.validation.Validation
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class LoginPresenter @Inject constructor(@Named("shared_preference") session: Session, validation: Validation)
    : BasePresenter<LoginContract.LoginView>(), LoginContract.LoginPresenter {


    private val session: Session = session
    private val validation: Validation = validation

    override fun onViewAttached() {
        if (session.isLoggedIn()) {
            getView()?.gotoChatActivity()
        }
    }

    override fun onViewDetached() {
        // no-op
    }

    override fun onClickLogin() {
        if (isFullNameOkay()) {
            saveUser(getView()?.getFullName()!!)
        }
    }

    override fun onTextChanged(text: CharSequence) {
        if (!validation.isNameAvailableForSignUp(text.toString().trim())) {
            getView()?.showErrorFullNameAlreadyTaken()
        } else {
            getView()?.hideError()
        }
    }

    override fun isEmpty(error: CharSequence?): Boolean {
        return validation.isNullOrEmpty(error)
    }

    private fun isFullNameOkay(): Boolean {
        if (validation.isEmpty(getView()?.getFullName()!!)) {
            getView()?.showErrorFullNameIsNull()
            return false
        }

        if (!validation.isNameValid(getView()?.getFullName()!!)) {
            getView()?.showErrorInvalidFullName()
            return false
        }

        if (!validation.isNameAvailableForSignUp(getView()?.getFullName()!!)) {
            getView()?.showErrorFullNameAlreadyTaken()
            return false
        }

        getView()?.hideError()
        return true
    }

    private fun saveUser(fullName: String) {
        session.saveUser(User.build(fullName))
        getView()?.gotoChatActivity()
    }
}