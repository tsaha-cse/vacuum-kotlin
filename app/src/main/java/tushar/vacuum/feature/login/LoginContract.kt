package tushar.vacuum.feature.login

import tushar.arch.BaseContract


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

interface LoginContract {

    interface LoginView : BaseContract.View {

        fun getFullName(): String

        fun gotoChatActivity()

        fun showErrorFullNameIsNull()

        fun showErrorInvalidFullName()

        fun showErrorFullNameAlreadyTaken()

        fun hideError()
    }

    interface LoginPresenter : BaseContract.Presenter<LoginView> {

        fun onClickLogin()

        fun onTextChanged(text: CharSequence)

        fun isEmpty(error: CharSequence?): Boolean
    }
}