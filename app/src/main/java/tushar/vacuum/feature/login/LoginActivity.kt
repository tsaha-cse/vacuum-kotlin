package tushar.vacuum.feature.login

import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import butterknife.BindView
import butterknife.OnClick
import butterknife.OnFocusChange
import butterknife.OnTextChanged
import tushar.vacuum.R
import tushar.vacuum.di.login.LoginModule
import tushar.vacuum.feature.base.BaseExtensionActivity
import tushar.vacuum.feature.base.open
import tushar.vacuum.feature.chat.ChatActivity
import javax.inject.Inject


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class LoginActivity : BaseExtensionActivity<LoginContract.LoginView, LoginContract.LoginPresenter>(), LoginContract.LoginView {

    @BindView(R.id.tiName)
    lateinit var tiName: TextInputLayout

    @BindView(R.id.etName)
    lateinit var etName: TextInputEditText

    @Inject
    lateinit var presenter: LoginPresenter

    override fun getLayoutResId(): Int {
        return R.layout.activity_login
    }

    override fun init() {
        supportActionBar?.let {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun initPresenter(): LoginPresenter {
        app.getAppComponent()
                .plus(LoginModule())
                .inject(this)
        return presenter
    }

    override fun retainPresenter(presenter: LoginContract.LoginPresenter?) {
        this.presenter = presenter as LoginPresenter
    }

    @OnFocusChange(R.id.etName)
    fun onFocusChanged(isFocused: Boolean) {
        if (isFocused) {
            etName.hint = getString(R.string.enter_your_full_name)
        } else {
            etName.hint = null
        }
    }

    @OnTextChanged(value = R.id.etName, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    fun onTextChanged(text: CharSequence) {
        presenter.onTextChanged(text)
    }

    override fun gotoChatActivity() {
        open<ChatActivity>()
        finish()
    }

    override fun getFullName(): String {
        return etName.text.toString().trim { it <= ' ' }
    }

    override fun showErrorFullNameIsNull() {
        tiName.error = getString(R.string.enter_your_full_name)
    }

    override fun showErrorInvalidFullName() {
        tiName.error = getString(R.string.invalid_full_name)
    }

    override fun showErrorFullNameAlreadyTaken() {
        tiName.error = getString(R.string.full_name_is_taken)
    }

    override fun hideError() {
        if (!presenter.isEmpty(tiName.error)) {
            tiName.error = null
        }
    }

    @OnClick(R.id.btnLogin)
    fun onClickLogin() {
        presenter.onClickLogin()
    }
}