package tushar.vacuum.feature.chat

import tushar.arch.BaseContract
import tushar.vacuum.model.Chat


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

interface ChatContract {

    interface ChatView : BaseContract.View {

        fun setResult(chats: List<Chat>)

        fun insertResult(chat: Chat)

        fun onLogout()
    }

    interface ChatPresenter : BaseContract.Presenter<ChatView> {

        fun getUserName(): String

        fun loadData(forceRefresh: Boolean)

        fun logout()
    }
}