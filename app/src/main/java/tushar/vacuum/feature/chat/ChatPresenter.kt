package tushar.vacuum.feature.chat

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import tushar.arch.BasePresenter
import tushar.vacuum.di.RunOn
import tushar.vacuum.di.SchedulerType.IO
import tushar.vacuum.model.Chat
import tushar.vacuum.repository.DataRepository
import tushar.vacuum.session.Session
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class ChatPresenter @Inject constructor(dataRepository: DataRepository, @RunOn(IO) ioScheduler: Scheduler,
                                        @RunOn(IO) uiScheduler: Scheduler,
                                        @Named("shared_preference") session: Session)
    : BasePresenter<ChatContract.ChatView>(), ChatContract.ChatPresenter {

    private val dataRepository: DataRepository = dataRepository
    private val ioScheduler: Scheduler = ioScheduler
    private val uiScheduler: Scheduler = uiScheduler
    private val session: Session = session

    private var disposeBucket: CompositeDisposable = CompositeDisposable()

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onPresent() {
        if (session.isLoggedIn()) {
            getView()?.showLoading()
            // set true to force update from server
            // in that case chats will be refreshed every time when
            // activity get restarted and local chats
            // will not persist
            loadData(false)
        } else {
            getView()?.onLogout()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onAbsent() {
        disposeBucket.clear()
    }

    override fun loadData(forceRefresh: Boolean) {
        val disposable = dataRepository.loadData(forceRefresh)
                .subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .subscribe(
                        { this@ChatPresenter.onSuccess(it) },
                        { this@ChatPresenter.onError(it) },
                        { this@ChatPresenter.getView()?.hideLoading() })
        disposeBucket.add(disposable)
    }

    override fun getUserName(): String {
        return session.getUser()?.fullName!!
    }

    private fun onSuccess(chats: List<Chat>) {
        getView()?.hideLoading()
        getView()?.setResult(chats)
    }

    private fun onError(throwable: Throwable) {
        getView()?.hideLoading()
        getView()?.showToast(if (throwable is IOException) "No Internet Connection" else throwable.message!!)
    }

    fun onClickSendMessage(content: String) {
        val ownChatMessage = Chat.build(getUserName(),
                content, System.currentTimeMillis().toString())
        val disposable = insertChat(ownChatMessage)
                .subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .subscribe(
                        { this@ChatPresenter.onSuccessInsert(ownChatMessage) },
                        { throwable -> this@ChatPresenter.onError(throwable) })
        disposeBucket.add(disposable)
    }

    private fun onSuccessInsert(ownChatMessage: Chat) {
        getView()?.insertResult(ownChatMessage)
    }

    private fun insertChat(ownChatMessage: Chat): Completable {
        return Completable.fromAction { dataRepository.addData(ownChatMessage) }
    }

    override fun logout() {
        val disposable = Completable.fromAction {
            session.flush()
            dataRepository.clearData()
        }.subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .subscribe({ this.onSuccessLogout() }, { this.onError(it) })
        disposeBucket.add(disposable)
    }

    private fun onSuccessLogout() {
        getView()?.onLogout()
    }
}