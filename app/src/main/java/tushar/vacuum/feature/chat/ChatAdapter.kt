package tushar.vacuum.feature.chat

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.makeramen.roundedimageview.RoundedImageView
import tushar.vacuum.R
import tushar.vacuum.feature.base.BaseRecyclerViewAdapter
import tushar.vacuum.feature.base.inflate
import tushar.vacuum.feature.base.setImageFromUrl
import tushar.vacuum.feature.base.tryDate
import tushar.vacuum.model.Chat
import tushar.vacuum.model.UserType
import java.security.InvalidParameterException

/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class ChatAdapter(context: Context, chats: MutableList<Chat>)
    : BaseRecyclerViewAdapter<Chat, RecyclerView.ViewHolder>(chats) {

    private val chats: MutableList<Chat> = chats
    private val context: Context = context
    private var clickListener: OnItemClickListener? = null

    fun setItemClickListener(clickListener: OnItemClickListener?) {
        this.clickListener = clickListener
    }

    override fun getItemViewType(position: Int): Int {
        return chats[position].userType
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == UserType.OTHER.getType()) {
            ChatOtherViewHolder(parent!!.inflate(R.layout.item_chat_other))
        } else {
            ChatOwnViewHolder(parent!!.inflate(R.layout.item_chat))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        if (holder is ChatOtherViewHolder) {
            holder.bind(position)
        } else {
            (holder as ChatOwnViewHolder).bind(position)
        }
    }

    override fun getItemCount(): Int {
        return chats.size
    }

    fun get(position: Int): Chat {
        if (position < 0 || position >= chats.size) {
            throw InvalidParameterException("Invalid item index")
        }
        return chats[position]
    }

    inner class ChatOtherViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.tvMessage)
        lateinit var titleMessage: TextView

        @BindView(R.id.tvInfo)
        lateinit var tvInfo: TextView

        @BindView(R.id.imgAvatar)
        lateinit var imgAvatar: RoundedImageView

        init {
            ButterKnife.bind(this, view)
        }

        fun bind(position: Int) {
            val chat: Chat = chats[position]
            titleMessage.text = chat.content?.trim()
            tvInfo.text = context.getString(R.string.chat_info, chat.username!!, formatRelativeTime(chat.time!!))

            if (!TextUtils.isEmpty(chat.userImageUrl.trim())) {
                imgAvatar.setImageFromUrl(chat.userImageUrl)
            }

            titleMessage.setOnClickListener {
                clickListener?.OnItemClick(position)
            }
        }
    }

    inner class ChatOwnViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.tvMessage)
        lateinit var titleMessage: TextView

        @BindView(R.id.tvInfo)
        lateinit var tvInfo: TextView

        init {
            ButterKnife.bind(this, view)
        }

        fun bind(position: Int) {
            val chat = chats[position]
            titleMessage.text = chat.content?.trim()
            tvInfo.text = formatRelativeTime(chat.time!!)
            titleMessage.setOnClickListener {
                clickListener?.OnItemClick(position)
            }
        }
    }

    private fun formatRelativeTime(time: String): String {
        return time.tryDate()
    }

    @FunctionalInterface
    interface OnItemClickListener {
        fun OnItemClick(position: Int)
    }

}