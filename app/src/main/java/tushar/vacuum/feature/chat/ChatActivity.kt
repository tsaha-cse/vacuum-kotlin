package tushar.vacuum.feature.chat

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import butterknife.OnTextChanged
import tushar.vacuum.R
import tushar.vacuum.di.chat.ChatModule
import tushar.vacuum.feature.base.BaseExtensionActivity
import tushar.vacuum.feature.base.open
import tushar.vacuum.feature.base.toast
import tushar.vacuum.feature.login.LoginActivity
import tushar.vacuum.feature.util.SoftKeyboardStateWatcher
import tushar.vacuum.model.Chat
import javax.inject.Inject


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

class ChatActivity : BaseExtensionActivity<ChatContract.ChatView, ChatContract.ChatPresenter>(), ChatContract.ChatView {

    @BindView(R.id.rvChat)
    lateinit var rvChat: RecyclerView

    @BindView(R.id.tvInfo)
    lateinit var tvInfo: TextView

    @BindView(R.id.etMessage)
    lateinit var etMessage: EditText

    @BindView(R.id.btnSend)
    lateinit var btnSend: Button

    private lateinit var chatAdapter: ChatAdapter

    @Inject
    lateinit var presenter: ChatPresenter

    override fun getLayoutResId(): Int {
        return R.layout.activity_chat
    }

    override fun init() {
        supportActionBar?.let {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
            it.title = presenter.getUserName()
            it.subtitle = getString(R.string.app_name)
        }

        rvChat.apply {
            chatAdapter = ChatAdapter(this@ChatActivity, mutableListOf())
            chatAdapter.setItemClickListener(object : ChatAdapter.OnItemClickListener {
                override fun OnItemClick(position: Int) {
                    toast(chatAdapter.get(position).content!!)
                }
            })
            layoutManager = LinearLayoutManager(applicationContext)
            rvChat.adapter = chatAdapter
            itemAnimator = DefaultItemAnimator()
        }

        val softKeyboardStateWatcher = SoftKeyboardStateWatcher(findViewById(R.id.root))
        softKeyboardStateWatcher.addSoftKeyboardStateListener(object : SoftKeyboardStateWatcher.SoftKeyboardStateListener {
            override fun onSoftKeyboardOpened(keyboardHeightInPx: Int) {
                if (chatAdapter.itemCount > 0) {
                    rvChat.scrollToPosition(chatAdapter.itemCount - 1)
                }
            }

            override fun onSoftKeyboardClosed() {
                if (chatAdapter.itemCount > 0) {
                    rvChat.scrollToPosition(chatAdapter.itemCount - 1)
                }
            }
        })
    }

    override fun initPresenter(): ChatContract.ChatPresenter {
        app.getAppComponent()
                .plus(ChatModule())
                .inject(this)
        return presenter
    }

    override fun retainPresenter(presenter: ChatContract.ChatPresenter?) {
        this.presenter = presenter as ChatPresenter
    }

    override fun setResult(chats: List<Chat>) {
        runOnUiThread {
            tvInfo.visibility = View.GONE
            chatAdapter.clear()
            chatAdapter.addAll(chats)
            rvChat.smoothScrollToPosition(chatAdapter.itemCount - 1)
        }
    }

    override fun insertResult(chat: Chat) {
        runOnUiThread {
            chatAdapter.add(chat)
            etMessage.text = null
            rvChat.scrollToPosition(chatAdapter.itemCount - 1)
        }
    }

    override fun onLogout() {
        open<LoginActivity>()
        finish()
    }

    @OnTextChanged(value = R.id.etMessage, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    fun onMessageTextChanged(message: CharSequence) {
        btnSend.isEnabled = message.toString().trim().isNotEmpty()
    }

    @OnClick(R.id.btnSend)
    fun onClickSendMessage() {
        presenter.onClickSendMessage(etMessage.text.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_chat, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_log_out) {
            presenter.logout()
        }
        return super.onOptionsItemSelected(item)
    }
}