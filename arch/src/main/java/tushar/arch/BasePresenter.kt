package tushar.arch

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.os.Bundle
import android.support.annotation.CallSuper


/**
 * Created by TUSHAR on 3/2/18.
 * tsaha.cse@gmail.com
 */

abstract class BasePresenter<V : BaseContract.View> : BaseContract.Presenter<V>, LifecycleObserver {

    private var mView: V? = null
    private var mStateBundle: Bundle? = null

    override fun attachLifecycle(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
    }

    override fun detachLifecycle(lifecycle: Lifecycle) {
        lifecycle.removeObserver(this)
    }

    override fun attachView(view: V) {
        this.mView = view
        onViewAttached()
    }

    override fun isViewAttached(): Boolean {
        return mView != null
    }

    override fun getView(): V? {
        return mView
    }

    override fun getStateBundle(): Bundle? {
        if (mStateBundle == null) {
            mStateBundle = Bundle()
        }
        return mStateBundle
    }

    override fun detachView() {
        mView = null
        onViewDetached()
    }

    @CallSuper
    override fun onPresenterDestroy() {
        if (mStateBundle != null && !mStateBundle!!.isEmpty) {
            mStateBundle!!.clear()
        }
    }

    override fun onPresenterCreated() {
        // no-op
    }

    override fun onViewAttached() {
        // no-op
    }

    override fun onViewDetached() {
        // no-op
    }
}