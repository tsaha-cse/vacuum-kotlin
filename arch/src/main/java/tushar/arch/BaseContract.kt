package tushar.arch

import android.arch.lifecycle.Lifecycle
import android.os.Bundle

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

interface BaseContract {

    interface View {

        fun showLoading()

        fun hideLoading()

        fun showToast(message: String)
    }

    interface Presenter<V : BaseContract.View> {

        fun isViewAttached() : Boolean

        fun getView(): V?

        fun getStateBundle(): Bundle?

        fun attachLifecycle(lifecycle: Lifecycle)

        fun detachLifecycle(lifecycle: Lifecycle)

        fun attachView(view: V)

        fun detachView()

        fun onPresenterCreated()

        fun onPresenterDestroy()

        fun onViewAttached()

        fun onViewDetached()
    }
}
