package tushar.arch

import android.arch.lifecycle.ViewModel

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

class BaseViewModel<V : BaseContract.View, P : BaseContract.Presenter<V>> : ViewModel() {

    private var presenter: P? = null

    fun setPresenter(presenter: P?) {
        if (this.presenter == null) {
            this.presenter = presenter
        }
    }

    fun getPresenter(): P? {
        return this.presenter
    }

    override fun onCleared() {
        super.onCleared()
        presenter?.onPresenterDestroy()
        presenter = null
    }
}
