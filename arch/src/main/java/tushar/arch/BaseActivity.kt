package tushar.arch

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v7.app.AppCompatActivity

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

abstract class BaseActivity<V : BaseContract.View, P : BaseContract.Presenter<V>> : AppCompatActivity(), BaseContract.View {

    private var lifecycleRegistry = LifecycleRegistry(this@BaseActivity)
    private var presenter: P? = null

    protected abstract fun initPresenter(): P?

    protected abstract fun retainPresenter(presenter: P?)

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initOrRetainPresenter()
    }

    private fun initOrRetainPresenter() {
        val viewModel = ViewModelProviders
                .of(this)
                .get(BaseViewModel::class.java)
                as BaseViewModel<V, P>
        var isPresenterCreated = false
        if (viewModel.getPresenter() == null) {
            viewModel.setPresenter(initPresenter())
            isPresenterCreated = true
        }
        presenter = viewModel.getPresenter()
        presenter?.attachLifecycle(lifecycle)
        presenter?.attachView(this as V)
        if (isPresenterCreated) {
            presenter?.onPresenterCreated()
        } else {
            retainPresenter(presenter)
        }
    }

    override fun getLifecycle(): LifecycleRegistry {
        return lifecycleRegistry
    }

    @CallSuper
    override fun onDestroy() {
        presenter?.detachLifecycle(lifecycle)
        presenter?.detachView()
        super.onDestroy()
    }
}
