package tushar.arch

import android.arch.lifecycle.LifecycleRegistry
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import android.view.View

/**
 * Created by TUSHAR on 2/16/18.
 * tsaha.cse@gmail.com
 */

abstract class BaseFragment<V : BaseContract.View, P : BaseContract.Presenter<V>> : Fragment(), BaseContract.View {

    private val lifecycleRegistry = LifecycleRegistry(this)
    protected var presenter: P? = null

    protected abstract fun initPresenter(): P?

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initOrRetainPresenter()
    }

    private fun initOrRetainPresenter() {
        val viewModel = ViewModelProviders
                .of(this)
                .get(BaseViewModel::class.java) as BaseViewModel<V, P>
        var isPresenterCreated = false
        if (viewModel.getPresenter() == null) {
            viewModel.setPresenter(initPresenter())
            isPresenterCreated = true
        }
        presenter = viewModel.getPresenter()
        presenter?.attachLifecycle(lifecycle)
        presenter?.attachView(this as V)
        if (isPresenterCreated) {
            presenter?.onPresenterCreated()
        }
    }

    override fun getLifecycle(): LifecycleRegistry {
        return lifecycleRegistry
    }

    @CallSuper
    override fun onDestroyView() {
        super.onDestroyView()
        presenter?.detachLifecycle(lifecycle)
        presenter?.detachView()
    }
}
